<?php

namespace Drupal\findit_library_sync\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 *
 * @MigrateProcessPlugin(
 *   id = "findit_library_get_services"
 * )
 */
class LibraryGetServices extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $raw_categories = $row->get($this->configuration['categories']);
    $raw_audience = $row->get($this->configuration['audience']);

    return $this->mapServices($raw_categories, $raw_audience, $migrate_executable, $row);
  }

  protected function mapServices($raw_categories, $raw_audience, MigrateExecutableInterface $migrate_executable, Row $row) {
    $categories = array_column($raw_categories, 'name');
    $audience = is_array($raw_audience) ? array_column($raw_audience, 'name') : [];

    $services_tids = [];

    foreach ($categories as $category) {
      switch ($category) {
        case 'Author Event':
        case 'Exhibit':
        case 'Film Screening':
          if (in_array('Adult', $audience)) {
            $services_tids[] = 118; // Adult Education.
          }
          break;

        case 'Book Group':
          if (in_array('Adult', $audience)) {
            $services_tids[] = 118; // Adult Education.
          }
          break;

        case 'City Event':
        case 'Program':
          $library_event_id = $row->get('src_unique_id');
          $migrate_executable->saveMessage("Library event with ID '$library_event_id' has the '$category' category. Skipping import operation.");
          throw new MigrateSkipRowException();
          break;

        case 'En español':
          $services_tids[] = 47; // Language Instruction.
          $services_tids[] = 118; // Adult Education.
          break;

        case 'ESOL':
          $services_tids[] = 47; // Language Instruction.
          break;

        case 'Parenting':
          $services_tids[] = 78; // Parenting Education.
          break;

        case 'Technology Class':
          if (in_array('Teen', $audience)) {
            $services_tids[] = 119; // Computer Access and Classes.
          }
          if (in_array('Adult', $audience)) {
            $services_tids[] = 119; // Computer Access and Classes.
          }
          break;

        case 'Workshop/Class':
          if (in_array('Adult', $audience)) {
            $services_tids[] = 118; // Adult Education.
          }
          break;
      }
    }

    return array_unique($services_tids);
  }

  /**
   * {@inheritdoc}
   */
  public function multiple() {
    return TRUE;
  }
}
