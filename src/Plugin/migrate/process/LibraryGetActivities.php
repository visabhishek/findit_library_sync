<?php

namespace Drupal\findit_library_sync\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 *
 * @MigrateProcessPlugin(
 *   id = "findit_library_get_activities"
 * )
 */
class LibraryGetActivities extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $raw_categories = $row->get($this->configuration['categories']);
    $raw_audience = $row->get($this->configuration['audience']);

    return $this->mapActivities($raw_categories, $raw_audience, $migrate_executable, $row);
  }

  protected function mapActivities($raw_categories, $raw_audience, MigrateExecutableInterface $migrate_executable, Row $row) {
    $categories = array_column($raw_categories, 'name');
    $audience = is_array($raw_audience) ? array_column($raw_audience, 'name') : [];

    $activities_tids = [];

    foreach ($categories as $category) {
      switch ($category) {
        case 'Author Event':
        case 'Book Group':
        case 'Exhibit':
        case 'Film Screening':
        case 'Social Event':
          $activities_tids[] = 67; // Community Events.

          if (in_array('Teen', $audience)) {
            $activities_tids[] = 241; // Academic Activities.
          }

          if (in_array('Children', $audience)) {
            $activities_tids[] = 70; // Young Children & Parent Activities.
          }
          break;


        case 'City Event':
        case 'Program':
          $library_event_id = $row->get('src_unique_id');
          $migrate_executable->saveMessage("Library event with ID '$library_event_id' has the '$category' category. Skipping import operation.");
          throw new MigrateSkipRowException();
          break;

        case 'Presentation/Lecture':
          $activities_tids[] = 67; // Community Events.
          break;


        case 'Sing-Along':
        case 'Storytime':
        case 'Workshop/Class':
          $activities_tids[] = 70; // Young Children & Parent Activities.
          break;

        case 'STEAM':
        case 'Technology Class':
          $activities_tids[] = 90; // STEAM (Science, Technology, Engineering, Arts, Math.
          break;

        case 'Summer Reading':
        case 'Workshop/Class':
          $activities_tids[] = 241; // Academic Activities.
          break;

      }
    }

    return array_unique($activities_tids);
  }

  /**
   * {@inheritdoc}
   */
  public function multiple() {
    return TRUE;
  }
}
